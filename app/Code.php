<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Code extends Model
{
    //
    protected $table = 'code';

    protected $guarded = ['id'];
}
