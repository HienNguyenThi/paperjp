<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Core\BaseRepository;
use App\Core\Admin\InnerAdmin;
use App\Http\Controllers\ValidateController;
use Illuminate\Support\Facades\Input;
use Auth;
use Response;
use Session;
use App\Code;
class AdminController extends Controller
{
    //
    protected $admin;
    protected $valid;

    public function __construct()
    {
        $this->admin = new InnerAdmin();
        $this->valid = new ValidateController();
    }

    public function index(Request $request){
        $users = $this->admin->getAllWithPaginate();


        return view('backend.admin.index', compact('users'));
    }

    

    public function logout(){
        Auth::guard('admin')->logout();
        if (!Auth::check() && !Auth::guard('admin')->check()) {
            Session::flush();
            Session::regenerate();
        }
        return redirect()->route('admin.login');
    }

    public function register(Request $request){
        if($request->isMethod('post')){
            $name       = (isset($request->name)) ? $request->name : '';
            $email      = (isset($request->email)) ? $request->email : '';
            $password   = (isset($request->password)) ? $request->password : '';

            if($this->valid->valueNotNull($name) && $this->valid->valueNotNull($email) && $this->valid->valueNotNull($password)){
                $data = [
                    'name' => $name,
                    'email' => $email,
                    'password' => bcrypt($password)
                ];

                $create = $this->admin->createAdmin($data);
                if($create == false){
                    return view('backend.register.index')->with(['message' => 'Register fail.', 'msgClass' => 'red']);
                }
                return view('backend.register.index')->with(['message' => 'Wating for admin approval.', 'msgClass' => 'green']);
            }else{
                return redirect()->route('admin.register');
            }
        }

        return view('backend.register.index');
    }

    public function activeUser(Request $request){
        if($request->ajax()){
            $input = Input::all();
            $status = $input['status'];
            $id = $input['id'];
            $param = [
                'status' => abs($status - 1),
            ];
            $codition = [
                'id' => $id
            ];
            $update = $this->admin->update($param, $codition);
    
            return Response::json($update, 200);
        }else{
            return Response::json('Not access', 500);
        }
    }
    
    public function changeRole(Request $request){
        if($request->ajax()){
            $input = Input::all();
            $role = $input['role'];
            $id = $input['id'];
            $param = [
                'role' => $role
            ];
            $codition = [
                'id' => $id
            ];
            $update = $this->admin->update($param, $codition);
    
            return Response::json($update, 200);
        }else{
            return Response::json('Not access', 500);
        }
    }
    
    public function delete(Request $request){
        if($request->ajax()){
            $input = Input::all();
            $id = $input['id'];
            $param = [
                'status' => -1
            ];
            $codition = [
                'id' => $id
            ];
            $update = $this->admin->update($param, $codition);
    
            return Response::json($update, 200);
        }else{
            return Response::json('Not access', 500);
        }
    }
    
}
