<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notify;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\UtilController;
use Config;
use Session;
use Response;

class NotifyController extends Controller
{
    public function __construct(){
        $this->notify = new Notify();
        $this->util = new UtilController();

    }

    public function index(Request $request){
        return view('backend.notify.index');
    }

    public function list($module,Request $request){
        switch($module){
            case 'new':
                $param = ['status' => 0];
                break;
            case 'posted':
                $param = ['status' => 1];
                break;
            case 'success':
                $param = ['status' => 2];
                break;
            case 'deleted':
                $param = ['status' => -1];
                break;
            default:
                $param = ['status' => 0];
        }

        $notifies = $this->notify->listNotify($param);
        // dd($notifies[0]);
        return view('backend.notify.manager', compact('notifies','module'));
    }

    public function createNotify(Request $request){
        if($request->isMethod('post')){
            $all     = Input::all();
            $heading = $all['heading'];
            $content = $all['content'];
            if($request->hasFile('image')){
                // file image
                $cusName = $this->util->generateRandomString(32);
                $imgName = $_FILES['image']['name'];
                $type = substr($imgName, strrpos($imgName, '.'));
                $cusName .= $type;
                $pathImg = Config::get('common.notify.path') . $cusName;
                Image::make($_FILES['image']['tmp_name'])->save($pathImg);
            }
            $notify = [
                'heading' => $heading,
                'content' => $content,
                'icon' => (isset($pathImg)) ? $cusName : ''
            ];

            $create = $this->notify->createNotify($notify);

            if($create){
                Session::flash('msg', 'success');
                return back();
            }else{
                Session::flash('msg', 'destroy');
                return back();
            }
        }
    }
    public function sendNotify(Request $request){
        if($request->ajax()){
            $notifies = $this->notify->list($request->id);
            if($notifies){
                $content      = array(
                    "en" => $notifies[0]->content
                );
                $heading      = array(
                        "en" => $notifies[0]->heading
                    );
            
                $hashes_array = array();
                $fields = array(
                        'app_id' => "9350ed9b-1a36-4040-858a-bbc9538cf9b0",
                        'included_segments' => array(
                            'All'
                        ),
                        'contents' => $content,
                        'headings' => $heading,
                        'chrome_web_icon' => 'https://www3.nhk.or.jp/news/html/20180822/../20180822/K10011586481_1808222151_1808222159_01_02.jpg'
                    );
                    
                    $fields = json_encode($fields);
                
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json; charset=utf-8',
                        'Authorization: Basic MjRmYWYxNmUtOGY4OC00NDkxLWIyYTktZTY4NTkzYWJhYzUx'
                    ));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($ch, CURLOPT_HEADER, FALSE);
                    curl_setopt($ch, CURLOPT_POST, TRUE);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                    
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $id = $request->id;
                    $status = $request->status;
                    $edit = $this->notify->changeStatus($id, $status);
                    if($edit){
                        return Response::json('success', 200);
                    }else {
                        return Response::json('No access', 302);
                    }
            }else {
                return Response::json('No access', 302);
            }
        }
    }

    public function changeStatus(Request $request){
        if($request->ajax()){
            $id = $request->id;
            $status = $request->status;
            $edit = $this->notify->changeStatus($id, $status);
            if($edit){
                return Response::json('success', 200);
            }else {
                return Response::json('No access', 302);
            }   
        }

    }

    public function editNotify(Request $request){
        if($request->isMethod('post')){
            $all = Input::all();
            $id = $all['id'];
            $heading = $all['heading'];
            $content = $all['content'];
            $oldImage = $all['oldImage'];
            if($request->hasFile('image')){
                // file image
                $cusName = $this->util->generateRandomString(32);
                $imgName = $_FILES['image']['name'];
                $type = substr($imgName, strrpos($imgName, '.'));
                $cusName .= $type;
                $pathImg = Config::get('common.notify.path') . $cusName;
                Image::make($_FILES['image']['tmp_name'])->save($pathImg);
            }
            $notify = [
                'id' => $id,
                'heading' => $heading,
                'content' => $content,
                'icon' => (isset($pathImg)) ? $cusName :  $oldImage
            ];
            $edit = $this->notify->editNotify($notify);
            if($edit){
                Session::flash('msg', 'success');
                return back();
            }else{
                Session::flash('msg', 'destroy');
                return back();
            }

        }

    }

}
