<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Version;
use Response;

class VersionController extends Controller
{
    //
    private $version;

    public function __construct()
    {
        $this->version = new Version();
    }

    public function delete(Request $request){
        if($request->ajax()){
            $id = $request->id;
            $del = $this->version->where('id', $id)->delete();
            if($del){
                return Response::json([
                    'status' => 200,
                    'message' => 'Success'
                ], 200);
            }else{
                return Response::json([
                    'status' => 302,
                    'message' => 'Không thể xoá.'
                ], 200);
            }
        }
    }

    public function edit(Request $request){
        if($request->ajax()){
            $type = $request->type;
            $id   = $request->id;
            $val  = $request->val;

            $version = $this->version->where('id', $id)->update([
                $type => $val
            ]);
            if($version){
                return Response::json([
                    'status' => 200,
                    'message' => 'Success'
                ], 200);
            }else{
                return Response::json([
                    'status' => 302,
                    'message' => 'Không update được version'
                ], 200);
            }
        }
    }

    public function version(){
        $versions = $this->version->get();

        $view = view('backend.version.index');
        $view->with('versions', $versions);

        return $view;
    }
}
