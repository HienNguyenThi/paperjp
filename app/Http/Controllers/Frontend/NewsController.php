<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ValidateController;
use App\Http\Controllers\UtilController;
use App\Http\Controllers\DictController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\News;
use App\Models\Sentence;
use Session;
use Response;
use Lang;

class NewsController extends Controller
{
    public $baseUrl = "http://mazii.net/";
    public $validate;

    private $limit = '/10';
    public $dict;

    public function __construct(){
        $this->get_news_url 	= $this->baseUrl."api/news/";
        $this->get_news_normal_url 	= $this->baseUrl."api/news_normal/";
    	$this->validate = new ValidateController();
        $this->util     = new UtilController();
        $this->dict     = new DictController();
        $this->news     = new News();
        $this->sent     = new Sentence();
    }
    function language(Request $request){
        Session::put('locale', $request->get('locale'));
       
        return redirect()->back ();
    }
    public function show(Request $request){
        $type = $request->type;
        if ($type == null) {
            $type = 'easy';
        }
        
        Session::put('type',$request->type);
        $top = array();
        // dd(Session::get('locale'));
        
        
            $lists     = $this->news->getTitle($type);
            
            $firstNews = $lists[0];
            if(!empty($firstNews->description)){
                $desc = $this->validate->replaceTagHTML($firstNews->description);
                if(!empty($desc)){
                    $desc = substr($desc, 0, 120);
                }
                
                $news['desc'] = $desc;
            }else{
                $news['desc'] = 'Easy News, Easy Japanese';
            }
    
            $title = $this->validate->getTitle($firstNews->title);
            if(!empty($title)){
                $news['title'] = $title . ' - Easy News | Easy Japanese';
            }else{
                $news['title'] = 'Easy News | Easy Japanese';
            }
            $news['url']	= urldecode($request->fullUrl());
          
            // get more 4 news show
            for($i = 0; $i < 10; $i++){
               
                $top[] = $lists[$i];
            }
            for ($j=0; $j < 5; $j++) { 
                $nav[] = $lists[$j];
                $nav[$j]->video = 'http://paperjp.local.com/video/'. $nav[$j]->video.'.mp4';
            }
        //    dd($nav);
            return view('frontend.news.index', compact('news', 'nav', 'top', 'home'));
        
    }
    
    public function showDetail(Request $request, $newsId)
    { 
        $type = Session::get('type');
        if ($type == null) {
            $type = 'easy';
        }
        // $result = $this->getTranslate($newsId);
        // $count = sizeof($result['data']);
        $detail = $this->news->getDetailNews($request->id);
        $detail = $detail[0];
        $id   = $request->id;
        $lists = $this->news->getTitle($type);
    // dd($detail);
        for ($j=0; $j < 5; $j++) { 
            $nav[] = $lists[$j];
        }
        
            $detail->video = 'http://paperjp.local.com/video/'. $detail->video.'.mp4';
        
        // if($type=='normal'){
        //     $detail->result->content->textbody = $detail->result->content->textbody .$detail->result->content->textmore;
            
        //     $detail->result->content->image = str_replace( '../', '/', $detail->result->content->image);
        //     $detail->result->content->image = 'http://www3.nhk.or.jp/news/html'.$detail->result->content->image;
        //     for($i = 0; $i < 10; $i++){
        //         $lists[$i]->value->image = str_replace('easy//..', 'html', $lists[$i]->value->image);
        //         $result[$i] = $this->getTranslate($newsId);
        //     }
            
        //     // remove div tag
        //     $detail->result->content->textbody = str_replace(['<div>', '</div>'], '', $detail->result->content->textbody);
        //     $detail->result->content->textbody = str_replace('<br><br>', '<br>', $detail->result->content->textbody);
        //     $detail->result->content->textbody = str_replace('<br>', '<p>', $detail->result->content->textbody);
        //     $detail->result->content->textbody = str_replace('</h3>', '</h3><p>', $detail->result->content->textbody);
        //     $detail->result->content->textbody = str_replace(['<strong>', '</strong>'], '', $detail->result->content->textbody);
        //     $active = true;
        // }
        // else{
        //     $home = true;
        //     $detail->result->content->textbody = str_replace(['<strong>', '</strong>'], '', $detail->result->content->textbody);
        // }
        // dd($detail->result->content->textbody);
        $urlImage = $this->validate->checkLink($detail->image);
        $detail->image = $urlImage;

        $url = route('web.dict');

        return view('frontend.news.detail', compact('detail', 'nav','page','id', 'url', 'active', 'home'));
    }


    public function getDictionary(Request $request){
        if($request->ajax()){
            $title = $this->validate->getTitle($request->text);
            $flag  = $request->type;
            $hira  = $request->hira;
            $tab   = $request->tab;
            $lang = Session::get('locale');
            $lang = "ja" .$lang;
            $url   ='http://mazii.net/api/search';
            if ($tab == 'word') {
                
                $data = [
                    "dict"  => $lang,
                    "type"  => "word",
                    "query" => $title,
                    "limit" => 15,
                    "page"  => 1
                ];
                $result = $this->util->postData($url, $data);
                $result = json_decode($result);
                $response = array();
                if(isset($result->found)){
                    if ($result->found == true || $result->status == 200) {
                        foreach ($result->data as $key => $value) {
                            $means = [];
                            if($flag == 'dict'){
                                $temp = true;
                            }elseif($flag == 'qsearch'){
                                $temp = ($value->word == $title && $value->phonetic == $hira);
                            }
                            if ($temp) {
                                $phonetic = $value->phonetic;
                                $word1    = $value->word;
                                foreach ($value->means as $k => $val) {
                                    if(isset($val->kind)){
                                        $dict = $val->kind;
                                        if($request->dict !== 'jako'){
                                            $val->kind = $this->dict->getKind($val->kind, $request->dict);  
                                        }
                                        $means[$dict][] = $val;
                                    }else{
                                        $val->kind = '';
                                        $means[$k][] = $val;
                                    }

                                    //check examples is null
                                    if(isset($val->examples)){
                                        if($val->examples == null){
                                            continue;
                                        }
                                        if($k == 0 && count($val->examples) < 1){
                                            $datExam = [
                                                "dict"  => $request->dict,
                                                "type"  => "example",
                                                "query" => $result->data[0]->word,
                                                "limit" => 1
                                            ];
                                            $resExam = $this->util->postData($url, $datExam);
                                            $resExam = json_decode($resExam);
                                            if($resExam->status == 200 && count($resExam->results)){
                                                $val->examples = $resExam->results;
                                            }
                                        }
                                    }
                                }       
                                $response[] = array(
                                        'phonetic' => $phonetic,
                                        'word'     => $word1,
                                        'means'    => $means 
                                );
                               
                            }
                        }
                        if(count($response)){
                            return Response::json(['found' => true, 'result' => $response], 200);
                        }else{
                            return Response::json(['found' => false], 302);
                        }
                    }
                    else{
                        return Response::json('Error',320);
                    }
                }else{
                    return Response::json('Error', 500);
                }
            }
            if($tab == 'example'){
                $query  = $request->text;
                $data = [
                    "dict"  => $lang,
                    "type"  => "example",
                    "query" => $query,
                    "limit" => 15,
                    "page"  => 1
                ];
                $result = $this->util->postData($url, $data);
                $resultSenten = json_decode($result);
                // dd($resultSenten);
                if($resultSenten->status == 200){
                    
                    return Response::json(['found' => true, 'result'=> $resultSenten], 200);
                }
                else{
                    return Response::json(['found' => false], 302);
                }
            }
            if ($tab == 'kanji') {
                if($lang !== 'javi'){
                    $lang ='jaen';
              
                }
                $query  = $request->text;
                $data = [
                    "dict"  => $lang,
                    "type"  => "kanji",
                    "query" => $query,
                    "limit" => 15,
                    "page"  => 1
                ];
                $result = $this->util->postData($url, $data);
                $result = json_decode($result);
                // dd($result);
                if($result->status == 200){
                    
                    return Response::json(['found' => true, 'result'=> $result], 200);
                }
                else{
                    return Response::json(['found' => false], 302);
                }
            }
            
        }
    }
    public function getSearch(Request $request){
        // $lists = $this->getHeadNews(1, '/10');       
        // return view('frontend.dictionary.search', compact('lists','result'));
        return view('frontend.dictionary.search');
    }
    public function searchData(Request $request){
        if ($request->ajax()) {
            $title = $this->validate->getTitle($request->text);
            $url   ='http://mazii.net/api/search';
            $data = [
                "dict"  => 'javi',
                "type"  => "kanji",
                "query" => $title,
                "limit" => 2,
                "page"  => 1
            ];

            $result = $this->util->postData($url, $data);
            $result = json_decode($result);
            return Response::json($result);
        }
    }
    public function getTranslate($newsId){
        $lang = Session::get('locale');
        $url   ='http://api.mazii.net/ej/api/news/'.$newsId.'/'.$lang.'/9';
        $result = json_decode(file_get_contents($url), true);
        return $result;
    }
    public function translate(Request $request, $newsId){
        $type = Session::get('type');
        if($type == 'normal'){
            $active = true;
        }else{
            $home = true;
        }
        // dd($home);
        $result = $this->getTranslate($newsId);
        $result = $result['data'];
        foreach($result as $key => $item){
            $temp = explode(':', $item['content']);
            array_shift($temp);
            $arr = [];
            foreach($temp as $index => $value){
                $a = preg_match('/"[0-9]"/', $value, $num);
                $i = (count($num)) ? $num[0] : '';
                $pStr = (strrpos($value, ',')) ? strrpos($value, ',') : ((strrpos($value, '.')) ? strrpos($value, '.') : false);
                if($pStr){
                    $str = substr($value, 0, $pStr);
                    $str = str_replace('"', '', $str);
                    if($i !== ''){
                        $i = str_replace('"', '', $i);
                        if($str !== ''){
                            $arr[$i-1] = trim($str);
                        }
                    }else{
                        if($str !== ''){
                            $arr[] = trim($str);
                        }
                    }
                }else{
                    $arr[] = trim(str_replace(['"', '}'], '', $value));
                }
            }
            $result[$key]['content'] = $arr;
        }
 
        $detail = $this->getDetailNews($newsId);
        if($detail->status == 200){
            $title = $detail->result->title;
            $content = trim($detail->result->content->textbody);
            $str = rtrim($content, '。');
            $str = explode('。', $str);
            array_unshift($str, $title);
            return view('frontend.news.translate',compact('str','result', 'home', 'active', 'newsId'));
        }
    }

    public function checkExistsTranslate(Request $request){
        $new_id = $request->id;
        $result = $this->news->getDetailNews($new_id);
        $user_id = Auth::guard('admin')->user()->id;
        $data = [];
        $title = $result[0]->title;
        
        $desc = $result[0]->description;
        $content = $result[0]->content;
        array_push($data, $title, $desc);
        $content = str_replace('<p>','', $content);
        $content = explode("<br>\n<br>\n",$content);
        for ($i=0; $i <sizeof($content); $i++) { 
            array_push($data, $content[$i]);
        }
        $list = $this->sent->listTrans($new_id);
        
         return view('frontend.news.translate', compact('data', 'new_id', 'list'));
        
    }

    public function addTranslate(Request $request){
        $id = $request->id;
        $user_id = Auth::guard('admin')->user()->id;
        $all = Input::all();
        // dd($all);
        $add = $this->sent->addSent($all, $user_id);
        
        return back();
    }

    public function getTrans(Request $request){
        $user = $request->user;
        $new = $request->new;
        
        $trans = $this->sent->getTrans($user, $new);
        return $trans;
    }
    public function getHeadNews($page , $limit, $type){
		if($page == null){
			$page = 1;
        }
        try{
            switch($type){
                case 'easy':
                    $url = $this->get_news_url.$page.$limit;
                    break;
                case 'normal':
                    $url = $this->get_news_normal_url.$page.$limit;
                    break;
                default:
                    $url = $this->get_news_url.$page.$limit;
            }
            $postData = @file_get_contents($url);
            if(empty($postData)){
                return null;
            }else{
                $data = json_decode($postData);
                $results = $data->results;
                foreach($results as $item){
                    if($this->validate->imageAvailable($item->value->image)){
                        $item->value->image = $this->validate->checkLink($item->value->image);
                    }
                }
                for ($i=0; $i < 10; $i++) { 
                    $newsId = $results[$i]->id;
                    $result[$i] = $this->getTranslate($newsId);
                    $count[$i] = sizeof($result[$i]['data']);
                    $results[$i]->count = $count[$i];
                }
                return $results;
            }
        }catch(\Exception $e){
            return null;
        }
    }

    

  
    
    function getTitleNews(Request $request){
        if($request->ajax()){
            $page = $request->page;
            $type = Session::get('type');
            $news = $this->getHeadNews($page, $this->limit, $type);
            if($news != null){
                return Response::json($news, 200);
            }
            return Response::json('Empty', 302);
        }
        return Response::json('error', 500);
    }

}
