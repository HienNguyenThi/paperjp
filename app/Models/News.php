<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    protected $table = 'news';

    protected $guarded  = ["id"];

    public $timestamps  = true;

    public function admins(){
        return $this->belongsTo('App\Models\Admin','user_id');
    }

    public function getTitle($type){
        $lists = News::select('id', 'title', 'view', 'description', 'kind', 'video')
        ->where('kind', $type)
        ->limit(12)
        ->get();
        return $lists;
    }

    public function getDetailNews($id){
        $detail = News::select('title', 'description','video','content', 'name_link')
        ->where('id', $id)
        ->get();
        return $detail;
    }
}
