<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Notify extends Model
{
    //
    protected $table = 'notifications';

    public function __construct(){
        DB::connection()->disableQueryLog();
    }

    public function createNotify($notify){
        return  Notify::insert([
            'heading' => $notify['heading'],
            'content' => $notify['content'],
            'icon' => $notify['icon'],
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }

    public function listNotify($param){
        return Notify::where('status',$param)->orderBy('created_at', 'DESC')->get();
    }

    public function list($id){
        return Notify::where('id',$id)->orderBy('created_at', 'DESC')->get();
    }

    public function changeStatus($id, $status){
        return Notify::where('id', $id)
                       ->update(['status' => $status,
                                 'updated_at' => date('Y-m-d H:i:s')]);
    }
    
    public function editNotify($notify){
        return Notify::where('id', $notify['id'])
                     ->update([
                        'heading' => $notify['heading'],
                        'content' => $notify['content'],
                        'icon' => $notify['icon'],
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
    }



}
