@extends('backend.layouts.master')

@section('breadcrumbs')
<section class="content-header">
    <h1>
        Trang chủ
        <small>Thông báo</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
        <li class="active">Thông báo</li>
        <li class="active">Tạo thông báo</li>
    </ol>
</section>
@endsection

@section('main-content')
<div class="box box-info">
    <div class="box-header">
      <h3 class="box-title">Thông báo mới</h3>
    </div>
    @if(Session::has('msg'))
    <div class="alert alert-{{Session::get('msg')}} alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>{{Session::get('msg')}}</strong>
    </div>
    @endif
    <form action="{{ route('admin.notify.create') }}" method="POST" name="form-news" enctype="multipart/form-data">
        <div class="box-body">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
            <div class="form-group">
                <label>Heading:</label>
                <input class="form-control none-resize auto-furi" placeholder="Heading..." id="heading"  name="heading">
            </div>
            <div class="form-group">
                <label>Content:</label>
                <input type="text" class="form-control" placeholder="Nội dung..." id="content"  name="content">
            </div>
            <div class="form-group">
                <label>Icon:</label>
                <input type="file" class="form-control" id="image"  name="image">
                <img id="iconNotify" class="col-md-7 iconNotify" src="">
            </div>
        </div>
        <div class="box-footer clearfix">
            <button type="submit" class="pull-right btn btn-primary" id="btn-send-news">Thêm thông báo
                <i class="fa fa-arrow-circle-right"></i></button>
        </div>
    </form>
</div>

@endsection
@section('after-script-end')
<script>
    function readURL(input) {            
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#iconNotify').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#image").change(function(){
            readURL(this);
        });
</script>

@endsection