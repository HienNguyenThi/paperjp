@extends('backend.layouts.master')

@section('breadcrumbs')
<section class="content-header">
    <h1>
        Trang chủ
        <small>Thông báo</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
        <li class="">Thông báo</li>
        <li class="active">Quản lý thông báo</li>
    </ol>
</section>
@endsection
@section('main-content')
<div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Danh sách.</h3>
        </div>
        <div class="box-body">
            <div class="margin-bottom">
                <form class="form-inline">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                    <label class="sr-only">
                        Trạng thái
                    </label>
                    <select class="form-control fillter_width_url">
                        <option {{ ($module == "new") ? "selected":"" }} 
                        value="new">Mới thêm</option>
                        <option {{ ($module == "posted") ? "selected":"" }} 
                        value="posted">Đã gửi</option>
                        <option {{ ($module == "deleted") ? "selected":"" }} 
                        value="deleted">Đã xóa</option>
                    </select>
                    </div>
                </form>
            </div>
            @if(Session::has('msg'))
            <div class="alert alert-{{Session::get('msg')}} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>{{Session::get('msg')}}</strong>
            </div>
            @endif
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped table-bordered table-hover tbl-vertical-middle">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tiêu đề</th>
                                <th>Nội dung</th>
                                <th>Icon</th>
                                <th style="width: 131px;">Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($notifies as $key => $item)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$item->heading}}</td>
                                    <td>{{$item->content}}</td>
                                    <td>{{$item->icon}}</td>
                                    <td>
                                        @if($item->status == 0)
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm btn-action-post" data-id="{{ $item->id }}" data-type="1" data-toggle="tooltip" title="Gửi tin nhắn"><i class="fa fa-upload"></i></button>
                                        <button type="button" class="btn btn-primary btn-sm" data-id="{{ $item->id }}" data-toggle="modal" data-target="#{{ $item->id }}"><i class="fa fa-pencil-square-o"></i></button>
                                            <button type="button" class="btn btn-danger btn-sm btn-action-notify" data-id="{{ $item->id }}" data-type="-1" data-toggle="tooltip" title="Xóa tin nhắn"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                        @elseif($item->status == -1)
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm btn-action-notify" data-id="{{ $item->id }}" data-type="0" data-toggle="tooltip" title="Khôi phục"><i class="fa fa-refresh"></i></button>
                                        </div>

                                        @endif
                                    </td>
                                </tr>
                                <div class="modal fade" id="{{ $item->id }}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="editNotifyLabel">Edit notify</h4>
                                                </div>
                                                <form class="form-horizontal" method="post" action="{{ route('admin.notify.edit') }}" enctype="multipart/form-data">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="id" id="id" value="{{$item->id}}">   
                                                    <input type="hidden" name="oldImage" id="oldImage" value="{{$item->icon}}">                        
                                                    <div class="modal-body">                                                               
                                                        <div class="row clearfix">
                                                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                                    <label for="title">Heading</label>
                                                                </div>
                                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                        <input type="text" id="heading" name="heading" class="form-control" value="{{$item->heading}}" placeholder="Enter your notify heading">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                                    <label for="name">Content</label>
                                                                </div>
                                                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <input type="text" id="content" name="content" class="form-control" value="{{$item->content}}" placeholder="Enter your notify content">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                                    <label for="image">Icon</label>
                                                                </div>
                                                                <div class="col-lg-5 col-md-5 col-sm-8 col-xs-7">
                                                                    <div class="form-group">
                                                                        <div class="form-line">
                                                                            <input type="file" id="image" name="image" value="" class="form-control">
                                                                        </div>                                                                            
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 col-md-5 col-sm-8 col-xs-7">
                                                                    @if($item->icon)                                
                                                                        <img class="icon" id="icon" src="{{ url('backend/img/') }}/{{$item->icon}}" alt=" ">                            
                                                                    @else
                                                                        <img class="icon" id="icon" src="{{ url('backend/img/default-image.png') }}" alt=" ">  
                                                                    @endif

                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">Edit</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>                                    
                                                    </div>
                                                </form>                                                       
                                            </div>
                                        </div>
                                    </div>    
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!--Phân trang-->
            {{-- @include('backend.includes.pagination', ['data' => $news, 'appended' => ['search' => Request::get('search')]])
            <div class="clearfix"></div> --}}
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-detail">
            <div class="modal-dialog custom-modal-detail-news" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    
                </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
@endsection
@section('after-script-end')
<script>
    function readURL(input) {            
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#icon').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#image").change(function(){
            readURL(this);
        });
    $(function(){
        $('.btn-action-post').click(function(){
                var id = $(this).data('id');
                var status = $(this).data('type');
                var row = $(this).closest("tr");
                $.ajax({
                    url: '{{ route("admin.notify.sendNotify") }}',
                    type: 'post',
                    data: {
                        id: id,
                        status: status
                    },
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    success: function(res){
                        swal('Thành công.');
                        row.addClass('hidden');
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
        });

        $('.btn-action-notify').click(function(){
                var id = $(this).data('id');
                var status = $(this).data('type');
                var row = $(this).closest("tr");
                $.ajax({
                    url: '{{ route("admin.notify.changeStatus") }}',
                    type: 'post',
                    data: {
                        id: id,
                        status: status
                    },
                    headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
                    success: function(res){
                        swal('Thành công.');
                        row.addClass('hidden');
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
        });
    });

</script>

@endsection