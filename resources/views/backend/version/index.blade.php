@extends('backend.layouts.master')

@section('title', 'Admin-Users')

@section('after-script-end')
    <script>
        $(function(){
            $('.btn-delete').click(function(e){
                var id = $(this).data('id');
                if(confirm('Bạn có chắc muốn xoá version')){
                    $.ajax({
                        type:'post',
                        url:'{{route("admin.version.delete")}}',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: {
                            id: id
                        },
                        success:function(data){
                            if(data.status == 200){
                                location.reload();
                            }else{
                                alert(data.message);
                            }
                        }
                    });
                }
            })
            $('.edit').focusout(function(e){
                var val = $(this)[0].innerText;
                var type = $(this).data('type');
                var id = $(this).data('id');

                $.ajax({
                    type:'post',
                    url:"{{route('admin.version.edit')}}",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {
                        val: val,
                        type: type,
                        id: id
                    },
                    success:function(data){
                        if(data.status == 200){
                            location.reload();
                        }else{
                            alert(data.message);
                        }
                    }
                });
            })
        })
    </script>
@endsection
@section('breadcrumbs')
<section class="content-header">
    <h1>
        Version
    </h1>
</section>
@endsection

@section('main-content')
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Danh sách.</h3>
        <div class="box-tools">
            <form method="GET" action="">
                <div class="input-group" style="width: 200px;">
                    <input type="text" name="search" class="form-control input-sm pull-right" placeholder="Nhập tên">
                    <div class="input-group-btn">
                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped table-bordered table-hover tbl-vertical-middle">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Version Int</th>
                            <th>Version Current</th>
                            <th>Note</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($versions as $version)
                            <tr>
                                <td>{{$version->id}}</td>
                                <td contenteditable="true" data-type="name" data-id="{{$version->id}}" class="edit">{{$version->name}}</td>
                                <td contenteditable="true" data-type="version_int" data-id="{{$version->id}}" class="edit">{{$version->version_int}}</td>
                                <td contenteditable="true" data-type="version_current" data-id="{{$version->id}}" class="edit">{{$version->version_current}}</td>
                                <td contenteditable="true" data-type="note" data-id="{{$version->id}}" class="edit">{{$version->note}}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" style="float: right">
                                        {{-- <button type="button" class="btn btn-sm btn-primary btn-plus"><i class="fa fa-plus-square"></i></button> --}}
                                        <button type="button" class="btn btn-sm btn-danger btn-delete" data-id="{{$version->id}}"><i class="fa fa-trash-o"></i></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="modal-detail">
    <div class="modal-dialog custom-modal-detail-news" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            
        </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection