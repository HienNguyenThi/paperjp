<div id="footer">
        <div class="container-footer">
            <div class="foot-logo">NJP<a style="    font-size: 30px;">-News Japanese</a></div>
            <div class="download">
                    <a  target="_blank" title="Android">
                       <img class="icon" src="{{ url('frontend/images/icon_google_play.png') }}" alt="" >
                    </a>
                    <a  target="_blank" title="Ios">
                       <img class="icon" src="{{ url('frontend/images/icon_app_store.png') }}" alt="" >
                    </a>
            </div>
            <p class="f-right t-right">Design by HienNguyen</a></p>
            <p class="f-left">
                <div class="f-left-copy">Copyright &copy;&nbsp;2019 . All Rights Reserved. </div>            
            </p>
        </div>
    </div>