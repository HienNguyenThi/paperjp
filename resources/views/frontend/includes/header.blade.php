{{-- <div>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav collapse navbar-collapse" id="navbarCollapse">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
      </ul>
      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>
</div> --}}

<div id="header" >
    <div id="header-nav" class="d-flex flex-column flex-md-row align-items-center p-4 px-md-4">
        <button class="navbar-toggler btn-menu not-web" type="button" data-toggle="collapse" data-target="#nav-mobile" aria-controls="nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        
        <h1 id="logo" class="my-0 mr-md-auto font-weight-normal"> 
            <a href="{{ route('web.easy.news', ['type' => 'easy']) }}" class="navbar-brand">JPN</a>
        </h1>
        <nav class="navbar navbar-expand-md navbar-dark">
            <ul id="nav" class="navbar-nav flex-row ml-md-auto d-none d-md-flex collapse navbar-collapse">
                <li class="nav-item {{(Request::is('news/easy*')|| isset($home)) ? 'active' : ''}}"><a class="nav-link" href="{{ route('web.easy.news', ['type' => 'easy']) }}">Easy News</a></li>
                <li class="nav-item {{ (Request::is('news/normal*')|| isset($active)) ? 'active' : ''}}"><a class="nav-link" href="{{ route('web.normal.news', ['type' => 'normal']) }}">Normal News</a></li>
                <li class="nav-item {{Request::is('dictionary*') ? 'active' : ''}}"><a class="nav-link" href="{{ route('web.search')}}">Dictionary</a></li>
                <li class="nav-item n">
                    <a href="#" class="bg-color nav-setting"><i class="fas fa-cog icon-setting"></i></a>
                    <ul class="sub-menu">
                        <li><a class="sub-hira bg-color" href="#">
                            {{-- <img class="icons-setting" src="{{ url('frontend/images/hiragana.png') }}"> --}}
                            <p class="bg-color setting-furi-mb">{{ trans('lable.list_setting.furi') }}</p>
                            <select class="sl-custom select-showFuri">
                                <option value="jlpt-n1">N1</option>
                                <option value="jlpt-n2">N2</option>
                                <option value="jlpt-n3">N3</option>
                                <option value="jlpt-n4">N4</option>
                                <option value="jlpt-n5">N5</option>
                            <option value="jlpt-all">{{ trans('lable.hidden') }}</option>
                            </select></a>
                        </li>
                    <hr class="hr-setting">
                        <li><a class="bg-color"  href="#">
                            {{-- <img class="icons-setting" src="{{ url('frontend/images/underline.png') }}"> --}}
                            <p class="bg-color-underline setting-underline">{{ trans('lable.list_setting.underline') }}</p>
                            <select class="sl-custom select-show-underLine">
                                <option value="jlpt-n1">N1</option>
                                <option value="jlpt-n2">N2</option>
                                <option value="jlpt-n3">N3</option>
                                <option value="jlpt-n4">N4</option>
                                <option value="jlpt-n5">N5</option>
                                <option value="jlpt-all">{{ trans('lable.hidden') }}</option>
                            </select></a>
                        </li>
                        <hr class="hr-setting">
                        <li id="language"><a class="bg-color"   id="dropdownMenuLink">
                            <p class="bg-color setting-language">{{ trans('lable.list_setting.language') }}</p>
                            <select class="sl-custom select-show-language" onchange="location = this.value;">
                                <option value="{{ route('lang', ['locale' => 'en']) }}">English</option>
                                <option value="{{ route('lang', ['locale' => 'vi']) }}">Vietnamese</option>
                            </select></a>
                        </li>
                        <hr class="hr-setting">
                        <li><a class="bg-color"  href="#">
                            {{-- <img class="icons-setting" src="{{ url('frontend/images/font-size.png') }}"> --}}
                            <p class="bg-color setting-size">{{ trans('lable.list_setting.size') }}</p>
                            
                            <select class="size-options">
                                <option value="50%" class="size">12</option></option>
                                <option value="60%" class="size">13</option>
                                <option value="70%" class="size">14</option>
                                <option value="80%" class="size">15</option>
                                <option value="90%" class="size">16</option>
                                <option value="95%" class="size">17</option>
                            </select></a>
                            
                        </a>
                        </li>
                        
                    </ul>
                </li>   
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ url('backend/dist/img/user2-160x160.jpg') }}" class="user-image">
                        <span class="hidden-xs">{{ Auth::guard('admin')->user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                        <img src="" class="img-circle" alt="User Image">

                        <p>
                            Admin - {{ Auth::guard('admin')->user()->name }}
                            <small>{{ Auth::guard('admin')->user()->created_at }}</small>
                        </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                        <div class="pull-left">
                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>  
 
    <ul id="nav-mobile" class="not-web collapse ">
        <li class="nav-item {{(Request::is('news/easy*')|| isset($home)) ? 'active' : ''}}"><a class="nav-link" href="{{ route('web.easy.news', ['type' => 'easy']) }}">Easy News</a></li>
        <li class="nav-item {{ (Request::is('news/normal*')|| isset($active)) ? 'active' : ''}}"><a class="nav-link" href="{{ route('web.normal.news', ['type' => 'normal']) }}">Normal News</a></li>
        <li class="nav-item {{Request::is('dictionary*') ? 'active' : ''}}"><a class="nav-link" href="{{ route('web.search')}}">Dictionary</a></li>
        <li class="nav-item n box-setting">
            <a href="#" class="nav-link">Setting</a>
            <ul class="sub-menu">
                <li><a class="sub-hira bg-color" href="#">
                    {{-- <img class="icons-setting" src="{{ url('frontend/images/hiragana.png') }}"> --}}
                    <p class="bg-color setting-furi-mb">{{ trans('lable.list_setting.furi') }}</p>
                    <select class="sl-custom select-showFuri-mb">
                        <option value="jlpt-n1">N1</option>
                        <option value="jlpt-n2">N2</option>
                        <option value="jlpt-n3">N3</option>
                        <option value="jlpt-n4">N4</option>
                        <option value="jlpt-n5">N5</option>
                        <option value="jlpt-all">{{ trans('lable.hidden') }}</option>
                    </select></a>
                </li>
            <hr class="hr-setting">
                <li><a class="bg-color"  href="#">
                    {{-- <img class="icons-setting" src="{{ url('frontend/images/underline.png') }}"> --}}
                    <p class="bg-color-underline setting-underline-mb">{{ trans('lable.list_setting.underline') }}</p>
                    <select class="sl-custom select-show-underLine-mb">
                        <option value="jlpt-n1">N1</option>
                        <option value="jlpt-n2">N2</option>
                        <option value="jlpt-n3">N3</option>
                        <option value="jlpt-n4">N4</option>
                        <option value="jlpt-n5">N5</option>
                        <option value="jlpt-all">{{ trans('lable.hidden') }}</option>
                    </select></a>
                </li>
                <hr class="hr-setting">
                <li id="language"><a class="bg-color"   id="dropdownMenuLink">
                    <p class="bg-color setting-language-mb">{{ trans('lable.list_setting.language') }}</p>
                    <select class="sl-custom select-show-language-mb" onchange="location = this.value;">
                        <option value="{{ route('lang', ['locale' => 'en']) }}">English</option>
                        <option value="{{ route('lang', ['locale' => 'ko']) }}">Korea</option>
                        <option value="{{ route('lang', ['locale' => 'vi']) }}">Vietnamese</option>
                        <option value="{{ route('lang', ['locale' => 'cn']) }}">China</option>
                    </select></a>
                </li>
                {{-- <li class="language-setting">
                    <p class="bg-color setting-language">{{ trans('lable.list_setting.language') }} </p>
                    <ul class="header-language">
                    <a class="language_action"><span style="    width: 192px;
                        margin-right: 8px;">{{ $curLang[Session::get('locale')]  }}</span>  <span style="font-size: 9px;
color: #45a2ff;">&#9660;</span> </a>

                    @foreach ($lang as $key => $value)
                        <a href="{{ route('lang', ['locale' => $key]) }}" class="item-language">
                            {{ $value }}
                        </a>
                    @endforeach
                </ul>
                </li> --}}
                <hr class="hr-setting">
                <li><a class="bg-color"  href="#">
                    {{-- <img class="icons-setting" src="{{ url('frontend/images/font-size.png') }}"> --}}
                    <p class="bg-color setting-size-mb">{{ trans('lable.list_setting.size') }}</p>
                    
                    <select class="size-options-mb">
                        <option value="50%" class="size">14</option>
                        <option value="60%" class="size">16</option>
                        <option value="70%" class="size">18</option>
                        <option value="80%" class="size">20</option>
                        <option value="90%" class="size">22</option>
                        <option value="95%" class="size">24</option>
                    </select></a>
                    
                </a>
                </li>
                
            </ul>
        </li>        
    </ul>

</div>