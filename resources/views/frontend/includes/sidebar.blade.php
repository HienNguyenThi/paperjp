<!-- {!! HTML::style('frontend/css/index.css') !!} -->

<div id="aside" class="col-md-3">
    <h3 class="h3-bold">News Related</h3>
    <ul class="menu list-title-menu">
        @foreach($nav as $item)
        <li>
            <a class="list-sidebar" href="{{ route('web.detail',['id' => $item->id]) }}" onclick="saveLocalStorage('{{$item->id}}')" id='{{$item->id}}'>
                <div class="row" > 
                    <div class="col-md-5 img-sidebar">
                        <div class="img-bg-default">
                            <img src="{{ (!empty($item->value->image)) ? $item->value->image : './frontend/images/default-news.jpg' }}" alt="" class="f-left img-menu-news" />
                        <!-- <img class="country" src='{{( !($item->count == 0)) ? trans ('lable.link-country') : ''}}'><p class="count-first">{{ ( !($item->count == 0)) ? $item->count : ''}}</p> -->
                        </div>
                    </div>
                    <div class="col-md-7 link-sidebar" >{!! $item->title !!}</div>           
                </div>
            </a>
        </li>
        @endforeach
    </ul>
</div>