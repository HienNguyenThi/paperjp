
@yield('before-css')
    {!! HTML::style('frontend/css/main.css') !!}
    {!! HTML::style('frontend/css/skin.css') !!}
    
    @yield('after-css')
<form action="{{  route('checkLogin') }}">
  

  <div class="container">
    <label for="uname"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="email" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>

    <button type="submit">Login</button>
    <label>
      <input type="checkbox" checked="checked" name="remember"> Remember me
    </label>
    <p>Don't have an account? <a href="{{ route('register') }}">Register</a>.</p>
  </div>


</form>
