@extends('frontend.news.layout')

@section('after-script')
    {{-- {!! HTML::script('frontend/js/common.js') !!} --}}
    <script type="text/javascript">
        $(function(){
            var id = '{{$id}}';          
            $('#'+id).css({"background-color": "#d9d9d9"});
            var url = '{{route('web.dict')}}';
            clickDict(url);
            

        });

        var a = document.selection.createRange().text;
        console.log(a)
        
    </script>
@endsection

@section('main-content')
<div id="detail" class="col-md-8">
    <div class="row" id="img">
        @if(($detail->video)!=null)
            <video id="#" style="width: 100%; height: 100%;" preload="auto" src="{!! $detail->video !!}">
            </video>
        @else
            <img src="{!!$detail->image !!}" alt="" class="" />
        @endif
    </div>
    <div class="row">
        <div  id="h3-bold-detail">{!! $detail->title !!}</div>
    </div>
    <div class="row"> <p>{!! $detail->pubDate !!}</p></div>   
   
    <div class="row" id="text">{!! $detail->content !!}</div>
    <div class="row">
        <div class="col-md-6 col-md-offset-5">
            <!-- <button type="button" class="btn-dictionary btn-translate" data-id="{{ $id }}">{{ trans ('lable.translation')}}</button> -->
            <a class="btn-dictionary btn-translate" href="{{ route('web.news.ajax.translate', ['id'=>$id]) }}" data-id="{{ $id }}">{{ trans ('lable.translation')}}</a>
        </div>
    </div>
<!-- modal -->
@include('frontend.modal.dict')
@include('frontend.modal.translate')
</div> 

@endsection
